package spurdo.data;

public class Data {
	private ID id;
	private double angle;
	private PiecePosition piecePosition;
	private String lap;
	
	public ID getId() {
		return id;
	}
	public void setId(ID id) {
		this.id = id;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition = piecePosition;
	}
	public String getLap() {
		return lap;
	}
	public void setLap(String lap) {
		this.lap = lap;
	}
	
	
}
