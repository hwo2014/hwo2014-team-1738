package spurdo.data;

public class Lane {
	
	private String startLaneIndex;
	private String endLaneIndex;
	
	public String getStartLaneIndex() {
		return startLaneIndex;
	}
	public void setStartLaneIndex(String startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}
	public String getEndLaneIndex() {
		return endLaneIndex;
	}
	public void setEndLaneIndex(String endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}
	
	
}
