package spurdo.data;

public class PiecePosition {
	private String pieceIndex;
	private String inPieceDistance;
	private Lane lane;
	
	public String getPieceIndex() {
		return pieceIndex;
	}
	public void setPieceIndex(String pieceIndex) {
		this.pieceIndex = pieceIndex;
	}
	public String getInPieceDistance() {
		return inPieceDistance;
	}
	public void setInPieceDistance(String inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}
	public Lane getLane() {
		return lane;
	}
	public void setLane(Lane lane) {
		this.lane = lane;
	}
	
	
}
