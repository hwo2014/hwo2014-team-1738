package spurdo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import spurdo.data.Data;
import spurdo.data.Message;

import com.google.gson.Gson;

public class Main {
	
	private double throttle = 0;
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        
        System.out.println("Connected to " + host + ":" + port + " as " + botName + "/" + botKey);

        new Main(reader, writer, new Join(botName, botKey));
        
        socket.close();
        
        System.err.println("Connection closed");
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
      
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
            if (msgFromServer.msgType.equals("carPositions")) {
            	Message m = gson.fromJson(line, Message.class);
            	ajaVitunKovaa(m);
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
                System.err.println("PING!");
            }
        }
    }

    private void ajaVitunKovaa(Message m) {
    	
    	for (Data d : m.getData()){
    		
    		double a = d.getAngle();
    				
    		System.out.println("Angle: " + a);
    		System.out.println("Throttle: " + throttle);
    		System.out.println("------------------------------");
    		
    		// If angle is 0 we can somewhat safely throttle more
    		if (a == 0){
    			if(throttle < 1)
    				throttle += 0.1;
    		} else if (a > 10){
    			if(throttle >= 0)
    				throttle -= 0.1;
    		}
    		
    		send(new Throttle(throttle));
    	}
		
	}
    
	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}